package com.company;

import java.util.List;

public interface ImageDAO {
    void add(Imageable imageable);
    List<Imageable> findAll();
    void generateExampleDate();

}
