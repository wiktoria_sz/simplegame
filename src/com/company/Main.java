package com.company;

import com.sun.deploy.config.JfxRuntime;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main extends JFrame implements ActionListener {

    private List<Imageable> imageables = new ArrayList<>();
    private ImageDAO imageDAO;
    private JPanel jPanel;
    private ImageGenerator simpleImageGenerator;


    public ImageDAO getImageDAO() {
        return imageDAO;
    }

    public List<Imageable> getImageables() {
        return imageables;
    }

    public void setImageDAO(ImageDAO imageDAO) {
        this.imageDAO = imageDAO;
        imageDAO.generateExampleDate();
        setImageables(imageDAO.findAll());
    }

    public void setImageables(List<Imageable> imageables) {
        this.imageables = imageables;
    }

    public Main() {
        createFrameLayout();
        setImageDAO(new ExampleImgData());

        simpleImageGenerator = new SimpleImageGenerator(getImageDAO().findAll());

        try {
            imageables = simpleImageGenerator.generate();
        } catch (NoImagesException e) {
            e.printStackTrace();
        }

        List<JButton> buttons = new ArrayList<>();

        for (int i = 1; i <= 4; i++) {  // create buttons
            JButton jButton = new JButton("");
            ImageIcon imageIcon = new ImageIcon(imageables.get(i - 1).getFileName());
            jButton.setIcon(imageIcon);
            jButton.addActionListener(this );
            jButton.setName(imageables.get(i -1).getClass().getSimpleName());

            jPanel.add(jButton); // add to "container" jPanel
        }

        add(jPanel);  // add jPanel to main "container"
    }



    private void createFrameLayout() {
        setSize(500, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS)); // add layout so label is visible above the grid

        JLabel jLabel = new JLabel("Wskaż niepasujący obrazek.");
        jLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        jLabel.setForeground(Color.blue);
        add(jLabel);

        jPanel = new JPanel();
        jPanel.setLayout(new GridLayout(2, 2));
    }


    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main();
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JButton clickedButton = (JButton) e.getSource();
        int theNumberWithTheSameClass = 0;
        for(Imageable imageable : getImageables()) {
            if (imageable.getClass().getSimpleName().equals(clickedButton.getName())) {
                theNumberWithTheSameClass++;
            }
        }

        if (theNumberWithTheSameClass == 1){
            JOptionPane.showMessageDialog(this, "Gratulacje! Dobra odpowiedź.");
        }
        else {
            JOptionPane.showMessageDialog(this, "Zła odpowiedź.");
        }

    }
}
