package com.company;

import java.util.List;

public interface ImageGenerator {
    List<Imageable> generate() throws NoImagesException; // method to generate random Images
}
