package com.company;

public abstract class Animal implements Imageable{

    private String filename;

        public Animal(String filename) {
            this.filename = filename;
        }

        @Override
        public String getFileName() {
            return filename;
        }
}
