package com.company;

import java.util.*;

public class SimpleImageGenerator implements ImageGenerator {

    // it is a class which may be exchanged with a different one

    private List<Imageable> imageDaoList = new ArrayList<>();
    private Set<String> generatedClasses = new HashSet<>();

    public SimpleImageGenerator(List<Imageable> imageDaoList) {
        this.imageDaoList = imageDaoList;
    }

    @Override
    public List<Imageable> generate() throws NoImagesException {
        Random random = new Random();
        List<Imageable> generatedImages = new ArrayList<>();
        int generatedIndex = 0;
        String className;

        while (true) {
            generatedIndex = random.nextInt(imageDaoList.size());
            className = imageDaoList.get(generatedIndex).getClass().getSimpleName();
            if (!generatedClasses.contains(className))
                break;
        }
        for (Imageable imageable : imageDaoList) {
            if (imageable.getClass().getSimpleName().equals(className)) {
                generatedImages.add(imageable);
            }
        } // adds all elements of the same class as the class of drawn element

        // adds an element which doesn't fit with the puzzle
        while (true) {
            generatedIndex = random.nextInt(imageDaoList.size());
            if (!imageDaoList.get(generatedIndex).getClass().getSimpleName().equals(className)) {
                generatedImages.add(imageDaoList.get(generatedIndex));
                break;
            }
        }
        generatedClasses.add(className);

        return generatedImages;
    }


}
