package com.company;

import java.util.ArrayList;
import java.util.List;

public class ExampleImgData implements ImageDAO {

    private List<Imageable> imageDaoList = new ArrayList<>();


    @Override
    public void add(Imageable imageable) {
        imageDaoList.add(imageable);
    }

    @Override
    public List<Imageable> findAll() {
        return imageDaoList;
    }

    @Override
    public void generateExampleDate() {
        imageDaoList.add(new Cat("obrazki/kot1.jpg"));
        imageDaoList.add(new Cat("obrazki/kot2.jpg"));
        imageDaoList.add(new Cat("obrazki/kot3.jpg"));
        imageDaoList.add(new Dog("obrazki/pies1.jpg"));
        imageDaoList.add(new Dog("obrazki/pies2.jpg"));
        imageDaoList.add(new Dog("obrazki/pies3.jpg"));
    }
}
