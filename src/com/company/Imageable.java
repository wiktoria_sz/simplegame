package com.company;

public interface Imageable {
    String getFileName();
}
